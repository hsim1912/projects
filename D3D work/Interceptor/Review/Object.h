#include <string>
#include <cctype>
#include "string.h"
#include <iomanip>
#include <iostream>


using namespace std;

class list
{
public:
	// auto constructor / destructor
	list()
	{
		headptr = NULL;	
	};
	~list()
	{
		if (is_empty)
			return;
		node *temp = new (nothrow) list::node;
		temp = headptr;

		while (temp->next != NULL)
		{
			headptr = headptr->next;
			remove (temp->word);
			temp = headptr;
		}
		delete temp;			
	};
	// insert copy constructor

	// mod functions

	bool insert (string word);
	bool remove (string &word);
	int find (string word);
	bool retrieve (string word);
	bool is_empty();
	int get_count();
	void get_links (int address);
	void get_links (string word);


private:

	struct node
	{
		string word;
		node *next;
	};

	node *headptr;
};
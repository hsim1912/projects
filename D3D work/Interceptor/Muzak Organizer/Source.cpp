#include <iostream>
#include <fstream>
#include <io.h>
#include <direct.h>
#include <string>
#include <stdio.h>
#include <Windows.h>

using namespace std;
// grab file name that ends with *.mp3
// chop to place 1 before - such as MGMT - Kids = MGMT
// make new into a directory if it does not yet exist.
// copy file to directory, scan next.

void get_name(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc);
bool check_dirs(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc);
bool move_file_found(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc);
bool move_file_notfound(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc);

int main()
{
	// variables for file names and locations of file and directory
	char *dirLoc, *curLoc;
	
	int hyphenLoc;
	string name;
	intptr_t dir_handle;
	_finddata_t all_files;
	// set initials to running library
	dirLoc = curLoc = _getcwd(NULL, 0);

	dir_handle = _findfirst("*", &all_files);

	get_name(name, dir_handle, all_files, curLoc);

	cout << "Organization Complete." << endl;
	return 0;
}

void get_name(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc)
{
	char *fileName;
	do
	{
		
		fileName = all_files.name;
		name = fileName;
		if (name.find(" -") != string::npos)
		{
		name = name.substr(0,name.find(" -"));
		
		move_file_notfound(name, dir_handle, all_files, curLoc);
		}
		/*
		if (check_dirs(name, dir_handle, all_files, curLoc))
		{
			move_file_found(name, dir_handle, all_files, curLoc);
		}
		else move_file_notfound(name, dir_handle, all_files, curLoc);	
		}
		_chdir("..");*/
	}while (_findnext (dir_handle, &all_files) == 0);


}
bool check_dirs(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc)
{
	int found =0;
	char *fileName;
	string foundName;
	// take name and check against locations within directory for name match
	// as well as type of directory.
	do
	{
		fileName = all_files.name;
		foundName = fileName;
		if (foundName.compare(name) == 0)
		found = 1;
	}while (_findnext (dir_handle, &all_files) == 0);
	
	return found;
}
bool move_file_found(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc)
{
	// file names
	char *testName;
	char *fName = all_files.name;
	// destination name
	char *dName;
	int len; // length of name
	// append directory tag
	name+="\\";
	// append file name.
	name+=fName;
	
	// copy name into char array for rename
	//strcpy(dName, name.c_str());
	len = name.size();

	dName = new (nothrow) char [len];
	//dName = name;

	// move file.
	if (rename(fName, name.c_str()) == 0)
	// check for success
	return 1;
	else return 0;
}
bool move_file_notfound(string name, intptr_t dir_handle, _finddata_t all_files, char *curLoc)
{
	_mkdir( name.c_str());
	if (move_file_found(name, dir_handle, all_files, curLoc) == 1)
		return 1;
	else return 0;
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using W2W.Model;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace W2W.ViewModel
{
    class Wardrobe : BindableBase
    {

        #region Atributes, Members, Properties

        private ObservableCollection<Clothing> _ClothingList;
        public ObservableCollection<Clothing> ClothingList
        {
            get
            {
                return _ClothingList;
            }
            set
            {
                _ClothingList = value;
            }

        }
        // Sorted type property access
        List<ObservableCollection<Clothing>> IsColorClothing;
        List<ObservableCollection<Clothing>> CanColorClothing;
        List<ObservableCollection<Clothing>> TypeClothing;
        List<ObservableCollection<Clothing>> SettingClothing;
        List<ObservableCollection<Clothing>> AccessoryClothing;
        private ObservableCollection<string> _ListOfAttrib;
        public ObservableCollection<string> ListOfAttrib
        {
            get
            {
                return _ListOfAttrib;
            }
            set
            {
                _ListOfAttrib = value;
            }
        }

        #endregion

        #region Constructor
        public Wardrobe()
        {
            ClothingList = new ObservableCollection<Clothing>();
            Clothing test = new Clothing();
            ListOfAttrib = new ObservableCollection<string>();

            test.CanRed = test.CanGrey = true;
            test.IsBlue = true;
            test.IsShiny = test.IsSmooth = true;
            test.Description = "Nice Sleek and completely a bs piece of clothes";
            test.IsClean = true;
            test.Type = 8;
            test.IsSetting = 4;

            ClothingList.Add(test);

            test = new Clothing();
            test.CanBlack = test.CanGrey = true;
            test.IsBlue = test.IsBlack = true;
            test.IsFuzzy = test.IsCombo = true;
            test.Description = "Nice Raiders top";
            test.Type = 3;
            test.IsSetting = 0;
            ClothingList.Add(test);

            ListOfAttrib.Add("Is:");
            ListOfAttrib.Add("Can Work With:");
            ListOfAttrib.Add("Type:");
            ListOfAttrib.Add("Settings:");
            ListOfAttrib.Add("Accessories:");
            ListOfAttrib.Add("Hamper:");
        }

        #endregion
    }
}

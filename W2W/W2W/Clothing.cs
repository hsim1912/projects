﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace W2W.Model
{
    class Clothing : BindableBase
    {

        #region Atributes, Members, Properties

        // Attributes:
        #region is
        private long _Id;
        public long Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }


        // Is
        private bool _IsBlack;
        public bool IsBlack
        {
            get
            {
                return _IsBlack;
            }
            set
            {
                _IsBlack = value;
            }
        }
        private bool _IsBlue;
        public bool IsBlue
        {
            get
            {
                return _IsBlue;
            }
            set
            {
                _IsBlue = value;
            }
        }
        private bool _IsGreen;
        public bool IsGreen
        {
            get
            {
                return _IsGreen;
            }
            set
            {
                _IsGreen = value;
            }
        }
        private bool _IsYellow;
        public bool IsYellow
        {
            get
            {
                return _IsYellow;
            }
            set
            {
                _IsYellow = value;
            }
        }
        private bool _IsWhite;
        public bool IsWhite
        {
            get
            {
                return _IsWhite;
            }
            set
            {
                _IsWhite = value;
            }
        }
        private bool _IsGrey;
        public bool IsGrey
        {
            get
            {
                return _IsGrey;
            }
            set
            {
                _IsGrey = value;
            }
        }
        private bool _IsRed;
        public bool IsRed
        {
            get
            {
                return _IsRed;
            }
            set
            {
                _IsRed = value;
            }
        }
        private bool _IsPurple;
        public bool IsPurple
        {
            get
            {
                return _IsPurple;
            }
            set
            {
                _IsPurple = value;
            }
        }
        private bool _IsOrange;
        public bool IsOrange
        {
            get
            {
                return _IsOrange;
            }
            set
            {
                _IsOrange = value;
            }
        }
        private bool _IsPink;
        public bool IsPink
        {
            get
            {
                return _IsPink;
            }
            set
            {
                _IsPink = value;
            }
        }
        private bool _IsLight;
        public bool IsLight
        {
            get
            {
                return _IsLight;
            }
            set
            {
                _IsLight = value;
            }
        }
        private bool _IsDark;
        public bool IsDark
        {
            get
            {
                return _IsDark;
            }
            set
            {
                _IsDark = value;
            }
        }
        private bool _IsBrown;
        public bool IsBrown
        {
            get
            {
                return _IsBrown;
            }
            set
            {
                _IsBrown = value;
            }
        }
        private string _Description;
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }
        // Wearable?
        private bool _IsClean;
        public bool IsClean
        {
            get
            {
                return _IsClean;
            }
            set
            {
                _IsClean = value;
            }
        }

        // _Is (Texture)
        private bool _IsShiny;
        public bool IsShiny
        {
            get
            {
                return _IsShiny;
            }
            set
            {
                _IsShiny = value;
            }
        }
        private bool _IsSmooth;
        public bool IsSmooth
        {
            get
            {
                return _IsSmooth;
            }
            set
            {
                _IsSmooth = value;
            }
        }
        private bool _IsFuzzy;
        public bool IsFuzzy
        {
            get
            {
                return _IsFuzzy;
            }
            set
            {
                _IsFuzzy = value;
            }
        }
        private bool _IsCombo;
        public bool IsCombo
        {
            get
            {
                return _IsCombo;
            }
            set
            {
                _IsCombo = value;
            }
        }

        private int _Type;
        public int Type
        {
            get
            {
                return _Type;
            }
            
            //0 Accessory,
            //1 UnderGarmet,
            //2 Hosiery,
            //3 Top,
            //4 Bottom,
            //5 Jacket, 
            //6 Shoe,
            //7 Hat,
            //8 OnePiece
            set
            {
                _Type = value;
            }
        }


        private int _IsSetting;
        public int IsSetting
        {
            get
            {
                return _IsSetting;
            }
            //0 Relaxed, 
            //1 Everyday, 
            //2 Casual, 
            //3 Business,
            //4 Dress, 
            //5 SpecialOccasions
            set
            {
                _IsSetting = value;
            }
        }


        // Is (Accessory Type):
        private int _AccType;
        public int AccType
        {
            get
            {
                return _AccType;
            }
            //0 None
            //1 WristJewelry, 
            //2 FootJewelery, 
            //3 Necklaces, 
            //4 EarRings, 
            //5 OtherFaceJewelery, 
            //6 OtherBodyJewelery, 
            //7 WaistItems, 
            //8 HandJewelery 
            set
            {
                _AccType = value;
            }
        }
        

        #endregion

        #region can
        // Can (modifiers)
        private bool _CanBlack;
        public bool CanBlack
        {
            get
            {
                return _CanBlack;
            }
            set
            {
                _CanBlack = value;
            }
        }
        private bool _CanBlue;
        public bool CanBlue
        {
            get
            {
                return _CanBlue;
            }
            set
            {
                _CanBlue = value;
            }
        }
        private bool _CanGreen;
        public bool CanGreen
        {
            get
            {
                return _CanGreen;
            }
            set
            {
                _CanGreen = value;
            }
        }
        private bool _CanYellow;
        public bool CanYellow
        {
            get
            {
                return _CanYellow;
            }
            set
            {
                _CanYellow = value;
            }
        }
        private bool _CanWhite;
        public bool CanWhite
        {
            get
            {
                return _CanWhite;
            }
            set
            {
                _CanWhite = value;
            }
        }
        private bool _CanGrey;
        public bool CanGrey
        {
            get
            {
                return _CanGrey;
            }
            set
            {
                _CanGrey = value;
            }
        }
        private bool _CanRed;
        public bool CanRed
        {
            get
            {
                return _CanRed;
            }
            set
            {
                _CanRed = value;
            }
        }
        private bool _CanPurple;
        public bool CanPurple
        {
            get
            {
                return _CanPurple;
            }
            set
            {
                _CanPurple = value;
            }
        }
        private bool _CanOrange;
        public bool CanOrange
        {
            get
            {
                return _CanOrange;
            }
            set
            {
                _CanOrange = value;
            }
        }
        private bool _CanPink;
        public bool CanPink
        {
            get
            {
                return _CanPink;
            }
            set
            {
                _CanPink = value;
            }
        }
        private bool _CanLight;
        public bool CanLight
        {
            get
            {
                return _CanLight;
            }
            set
            {
                _CanLight = value;
            }
        }
        private bool _CanDark;
        public bool CanDark
        {
            get
            {
                return _CanDark;
            }
            set
            {
                _CanDark = value;
            }
        }
        private bool _CanBrown;
        public bool CanBrown
        {
            get
            {
                return _CanBrown;
            }
            set
            {
                _CanBrown = value;
            }
        }

        enum CanSetting
        {
            Relaxed, Everyday, Casual, Business,
            Dress, SpecialOccasions
        }
#endregion

        // _Is (Env_Ironment)
        private int _IdealTemp;
        public int IdealTemp
        {
            get
            {
                return _IdealTemp;
            }
            set
            {
                _IdealTemp = value;
            }
        }

        #endregion

        #region Methods


        public void describe()
        {
                for (int i = 0; i < Description.Length; i++)
                    Console.WriteLine(Description[i]+ "\n");
        }
        

        public void wash()
        {
            IsClean = true;
        }

        public void worn()
        {
            IsClean = false;
        }

        private static int CompareID(Clothing x, Clothing y)
        {
            if (x.Id == y.Id)
                return 0;
            if (x.Id < y.Id)
                return -1;
            else return 1;
        }
        #endregion

        #region constructors

        public Clothing()
        {

        }

        #endregion
    }
}

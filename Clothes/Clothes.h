#include <iostream>
#include <fstream>
#include <cctype>
#include <string>

using namespace std;

class clothing
{
	// id
	int id;
	// is attributes
	int iBlack, iGrey, iWhite, iBrown, iBlue, iRed, iGreen, iYellow, iAcc,
		iPink, iPurple, iKahki, iRelaxed, iBusiness, iCasual, iFunc, iShoe, 
		iClean;
	// with attributes
	int wBlack, wGrey, wWhite, wBrown, wBlue, wRed, wGreen, wYellow, wAcc,
		wPink, wPurple, wKahki, wShoe;
	// avoid attributes
	int aBlack, aGrey, aWhite, aBrown, aBlue, aRed, aGreen, aYellow, aAcc,
		aPink, aPurple, aShoe;

	//types

	int minTemp, maxTemp, sun, outer, top, bottom, under, acc, one, shoe;

	string picLoc;

public:
	// verify functions
	int is_business();
	int is_casual();
	int is_func();
	int is_relax();
	int is_top();
	int is_bottom();
	int is_under();
	int is_acc();
	int is_shoe();
	int is_acc_able();
	int is_outer;
	int is_red();
	int is_blue();
	int is_green();
	int is_black();
	int is_brown();
	int is_grey();
	int is_yellow();
	int is_sun();
	
	//modifiers
	void make_dirty();
	void make_clean();
	void remove_clothing();
	void add_clothing();
	void change_temp();
};